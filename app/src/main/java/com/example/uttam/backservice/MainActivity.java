package com.example.uttam.backservice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    private BroadcastReceiver mBroadcastReceiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent startServiceIntent = new Intent(MainActivity.this, MyBackgroundService.class);
        startService(startServiceIntent);

        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
                    Intent pushIntent = new Intent(context, MyBackgroundService.class);
                    context.startService(pushIntent);
                //}
            }
            // your receiver implementation
        };
    }
}
