package com.example.uttam.backservice;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class MyBackgroundService extends Service {
    private PowerManager.WakeLock wl;

    public MyBackgroundService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        sleepPeriod();
        //Toast.makeText(this, "Invoke background service onCreate method.", Toast.LENGTH_LONG).show();
        super.onCreate();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "Invoke background service onStartCommand method.", Toast.LENGTH_LONG).show();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Invoke background service onDestroy method.", Toast.LENGTH_LONG).show();
    }

    public void sleepPeriod(){
        final Handler h=new Handler();
        final  Runnable r=new Runnable() {

            public void run() {
                turnOnScreen();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                addNotification();
                Toast.makeText(getBaseContext(),"Hello World !",Toast.LENGTH_SHORT).show();
                Log.d("TAG", "Hello World !");
            }
        };

        Timer t=new Timer();
        t.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                h.post(r);
            }
        },2000, 25000);
    }

    //turn on screen for 5 seconds
    public void turnOnScreen(){
        PowerManager pm = (PowerManager)getSystemService(Context.POWER_SERVICE);
        assert pm != null;
        wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "whatever");
        wl.acquire(5*1000L);
    }

    private void addNotification() {

        // int m=1;
        Random random = new Random();
        int m = random.nextInt(9999 - 1000) + 1000;

        Log.d("T", "Displaying Notification");
        Intent activityIntent = new Intent(this, NotificationView.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, activityIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setSmallIcon(R.drawable.synopi_icon_512);
        mBuilder.setColor(Color.GREEN);
        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setContentTitle("My Notification");
        mBuilder.setContentText("Content of Notification");
        mBuilder.setDefaults(Notification.DEFAULT_SOUND);
        //mBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
        mBuilder.setAutoCancel(true);

        NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(m, mBuilder.build());
    }


}
